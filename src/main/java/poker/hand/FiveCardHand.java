package poker.hand;

public class FiveCardHand {

    private Card[] pokerHand;
    private int[] dealtCards;
    public static final int numberOfCards = 5;

    public FiveCardHand()   {
        pokerHand = new Card[FiveCardHand.numberOfCards];
        dealtCards = new int[FiveCardHand.numberOfCards];
    }

    public Card[] getPokerHand() {
        return pokerHand;
    }

    public void setPokerHand(Card[] pokerHand) {
        this.pokerHand = pokerHand;
    }

    public int[] getDealtCards() {
        return dealtCards;
    }

    public void setDealtCards(int[] dealtCards) {
        this.dealtCards = dealtCards;
    }
}
