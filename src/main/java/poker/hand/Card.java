package poker.hand;

public class Card {

    private String suite;
    private String rank;

    public Card(String rank, String suite)  {
        this.suite = suite;
        this.rank = rank;
    }

    public String getSuite() {
        return suite;
    }

    public String getRank() {
        return rank;
    }

    public String toString()    {
        return rank + suite;
    }
}
