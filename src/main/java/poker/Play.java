package poker;

import poker.deal.Dealer;
import poker.deck.CardDeck;
import poker.evaluate.Evaluator;
import poker.factory.HandEvaluatorFactory;
import poker.hand.Card;
import poker.hand.FiveCardHand;

public class Play {

    public static void main(String[] args)  {
        CardDeck cardDeck = new CardDeck();
        cardDeck.buildDeck();
        Card[] deck = cardDeck.getDeck();
        System.out.println("Before shuffle");
        for(Card card : deck) {
            System.out.print(card.toString() + ":");
        }
        cardDeck.shuffle();
        deck = cardDeck.getDeck();
        System.out.println();
        System.out.println("After shuffle");
        for(Card card : deck) {
            System.out.print(card.toString() + ":");
        }
        FiveCardHand pokerHand = new FiveCardHand();
        System.out.println();
        Dealer.deal(cardDeck, pokerHand, FiveCardHand.numberOfCards);
        for(Card card : pokerHand.getPokerHand()) {
            System.out.println(card.toString());
        }
        Evaluator evaluator = HandEvaluatorFactory.getEvaluator(HandEvaluatorFactory.FIVE_CARD_DRAW);
        evaluator.evaluate(pokerHand.getPokerHand());
        System.out.println();
        cardDeck.shuffle();
        Dealer.deal(cardDeck, pokerHand, FiveCardHand.numberOfCards);
        for(Card card : pokerHand.getPokerHand()) {
            System.out.println(card);
        }
        evaluator.evaluate(pokerHand.getPokerHand());
    }
}
